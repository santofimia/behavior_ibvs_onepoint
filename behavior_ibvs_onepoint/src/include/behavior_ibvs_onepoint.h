#ifndef behavior_ibvs_onepoint
#define behavior_ibvs_onepoint

#include <behavior_process.h>
#include "libPID_control.h"
#include "detector.h"

/*!*************************************************************************************
 *  \class     IBVS_onepoint_process
 *
 *  \brief     IBVS with One Point Controller
 *
 *  \details   This process receives a position message through an input topic.  (Cambiar)
 *             If either the x or y values are negative, an error is reported.
 *             This process uses algorithms defined by the MyClass class.
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/dronePose.h"

#include<droneMsgsROS/ConsultBelief.h>
#include <aerostack_msgs/BehaviorEvent.h>



#include "sensor_msgs/Image.h"
#include "geometry_msgs/Point.h"
#include "sensor_msgs/CameraInfo.h"

#include <cv_bridge/cv_bridge.h>

#include "xmlfilereader.h"

class BehaviorIBVSOnePoint : public BehaviorProcess
{
public:

/*!*************************************************************************************
 * \details The function ownSetUp() is executed to setup everything that
 *          the process needs at the beginning such as read
 *          configuration files, read ROS parameters, etc.
 **************************************************************************************/
  void ownSetUp();

/*!*************************************************************************************
 * \details The function ownStart() prepares the process to start running.
 *          Connects topics and services and set the appropiate values.
 **************************************************************************************/
  void ownStart();

/*!*************************************************************************************
 * \details The function ownStop() perform shutdown of topics and services and
 *          reset values of variables.
 **************************************************************************************/
  void ownStop();

/*!*************************************************************************************
 * \details The function ownRun() the main body of execution to perform in the
 *          running state. Only used when the process is synchronous.
 **************************************************************************************/
  void ownRun();

  std::tuple<bool,std::string> ownCheckActivationConditions();

  void init();

  void ibvsPointCb(const geometry_msgs::Point &msg);
  void CamInfoCb(const sensor_msgs::CameraInfo &msg);
  void imageCb(const sensor_msgs::ImageConstPtr& msg);
  void droneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg);


  void passDynamic(double time);
  void IBVSOnePoint();


  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  BehaviorIBVSOnePoint();

  //! Destructor.
  ~BehaviorIBVSOnePoint();

private:

  PID PID_Dz;
  PID PID_Roll;
  PID PID_Dyaw;
  PID PID_Pitch;



  //! ROS NodeHandler used to manage the ROS communication
  ros::NodeHandle nIn;

  //! Configuration file variable
  std::string ibvsconfigFile;    // Config File String name
  std::string detectorconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name


  //! ROS publisher     Check
  ros::Publisher pub_attitude;
  ros::Publisher pub_daltitude;
  ros::Publisher pub_dyaw;
  ros::Publisher pub_image;
  ros::Publisher pub_dronecmd;

  droneMsgsROS::droneCommand drone_cmd_msg;

  ros::ServiceClient query_client;


  //! ROS Subscribers handler used to receive messages.
  ros::Subscriber image_sub;
//  ros::Subscriber point_sub;
  ros::Subscriber camera_info_sub;
  ros::Subscriber estimated_yaw_sub;

  std::string drone_id_;
  std::string image_receiver_topic_name;
  std::string camera_info_topic_name;
  std::string detector_image_topic_name;
  std::string drone_id_namespace;
  std::string drone_estimated_pose_topic_name;
  std::string pitchroll_cmd_topic_name;
  std::string daltitude_cmd_topic_name;
  std::string dyaw_cmd_topic_name;
  std::string ibvs_image_topic_name;
  std::string drone_command_highlevel_topic_name;

  cv::Point2f point;
  cv::Point2f ref_point;



  DynamicDetector dynamic_detector;

  // Point for pitch
  float image_dist;
  double timer;

  // adapt gains
  float factor_gains;
  float yaw;

  // Output
  float dz;
  float pitch;
  float roll;
  float dyaw;

  // reference
  float width_cam;
  float height_cam;

  // Gains

  float pid_dz_kp;
  float pid_dz_ki;
  float pid_dz_kd;
  bool pid_dz_enablesat;
  float pid_dz_satmax;
  float pid_dz_satmin;
  bool pid_dz_enableantiwp;
  float pid_dz_kw;
  float pid_dz_ffmass;
  float pid_dz_fffactor;
  bool pid_dz_enableff;

  float pid_roll_kp;
  float pid_roll_ki;
  float pid_roll_kd;
  bool pid_roll_enablesat;
  float pid_roll_satmax;
  float pid_roll_satmin;
  bool pid_roll_enableantiwp;
  float pid_roll_kw;

  float pid_dyaw_kp;
  float pid_dyaw_ki;
  float pid_dyaw_kd;
  bool pid_dyaw_enablesat;
  float pid_dyaw_satmax;
  float pid_dyaw_satmin;
  bool pid_dyaw_enableantiwp;
  float pid_dyaw_kw;

  float pid_pitch_kp;
  float pid_pitch_ki;
  float pid_pitch_kd;
  bool pid_pitch_enablesat;
  float pid_pitch_satmax;
  float pid_pitch_satmin;
  bool pid_pitch_enableantiwp;
  float pid_pitch_kw;

  float rotate_sign;

  bool centered;
  bool move;
  bool moving;
  bool done;
  bool hovering;
	
	float frames;

  bool detected;
  int iterator;
};
#endif
