#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>
#include <deque>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdlib>
#include <algorithm>
#include <string>
using namespace std;
using namespace cv;
class DynamicDetector;
template <class T>
class Buffer
{
  private:
    deque<T> elements;

    int n;

  public:
    Buffer(int n) { this->n = n; }
    Buffer() {n = 10;}
    void add(T a)
    {
        elements.push_front(a);
        if (elements.size() > n)
            elements.pop_back();
    }
    int size()  { return n; }
    bool empty()  { return elements.empty(); }
    bool filled()  { return elements.size() == n; }
    int numel()  { return elements.size(); }
    T get(int i)  { return elements[i]; }
    T top()  { return elements.front(); }
    T bottom()  { return elements.back(); }
};

class rotationTracker
{
    Buffer<Point> A;
    Buffer<Point> B;
    Buffer<Point> C;
     Buffer<float> adistance ;
    Buffer<float> bdistance ;
    Buffer<double> angleA, angleB, time;
    bool centerA;
    int history_size;
    bool ready = 0;
    void findCenter();
    void calculateW();
    double w;
    double T = -1, lastCycle = -1;
    int nLoops = 0;
    bool loopCounted;
    bool angleCheckPoint;

  public:
    rotationTracker(int n = 50) : history_size(n), adistance(n) , bdistance(n),C(n),A(n), B(n), angleA(n - 1), angleB(n - 1), time(n) {}
    void newDetection(Point a, Point b, Point anchor , double timestamp);
    void simulateDetection(Point a, double l, double timestamp);
    bool isReady()  { return ready; }
    double getW()
    {
        calculateW();
        return w;
    }
    double getT()  { return T; }
    Point getA(){ return A.top();}
    Point getB(){ return B.top();}

    double getAngle(double time);
    Point getCenter() { return centerA ? A.top() : B.top(); }
    Point getPoint() { return centerA ? B.top() : A.top(); }
            friend DynamicDetector;

};

class StickDetector
{
    void frame2bw();
    Point a, b;
    Mat frame, BW;
    int erosion = 1, dilation = 3;
    double l;
        double low_H_g = 36, low_S_g = 61.71, low_V_g = 52, high_H_g = 82, high_S_g = 255, high_V_g = 255; //GREEN LAST
        friend DynamicDetector;

    bool squared = 0, detected = 1;

  public:
    double getLenght() { return l; }
    void detect(Mat image);
    Point getA()  { return a; }
    Point getB()  { return b; }
    Point getCenter()  { return (a + b) / 2; }
    Point getLower()  { return (  (a.y<b.y) ?  a : b ); }
    int state() const
    {
        if (detected == 0)
            return 0;
        else
        {
            if (squared)
                return -1;
            else
                return 1;
        }
    } //returns 0 if not  detection was found, -1 if the detection is squared (annot see the full stick) and 1 if everything is ok;
};

class FrameDetector
{
    void frame2bw();
    Point p;
    Mat frame, BW, BW2;
    double low_H = 0, low_S = 104, low_V = 104, high_H = 32, high_S = 255, high_V = 255; //Orangepublic:
    // double low_H = 5, low_S = 200, low_V = 50, high_H = 30, high_S = 255, high_V = 255; //Orangepublic:
    bool detected = 0;
    friend DynamicDetector;
  public:
    void detect(Mat image, Point c);
    bool status() { return detected; }
    Point getP()  { return p; }
};
class DynamicDetector
{
  private:
    Point frameCorner, stickCenter, center, a,b;
    bool detectedFlag = 0;
    FrameDetector detectorOrange;
    StickDetector detectorGreen;
    rotationTracker tracker;
    Mat frame;
    Buffer<float> A ;
    Buffer<float> B ;
    Point centerMean ;

  public:
    DynamicDetector( string configfile);
    DynamicDetector(){}
    void nextFrame(Mat image, double timestamp);
    bool detected()  { return detectedFlag; }
    Point getCenter()  { return (tracker.getCenter() +detectorOrange.getP())/2; }
    Point getFrameCorner()  { return detectorOrange.getP(); }
    Point getA() {return tracker.getA();}
    Point getB() {return tracker.getB();}
    Point getRotationCenter()  { return tracker.getCenter(); }
    double getAngle(double time) { return tracker.getAngle(time); }
    double getW() { return tracker.getW(); }
    double getT() { return tracker.getT(); }
};
