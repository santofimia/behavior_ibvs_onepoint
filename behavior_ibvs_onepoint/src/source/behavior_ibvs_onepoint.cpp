#include "behavior_ibvs_onepoint.h"

//Constructor
BehaviorIBVSOnePoint::BehaviorIBVSOnePoint()  : BehaviorProcess()
{
    std::cout << "Constructor: BehaviorIBVSOnePoint" << std::endl;

}

//Destructor
BehaviorIBVSOnePoint::~BehaviorIBVSOnePoint() {}


void BehaviorIBVSOnePoint::init()
{

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~drone_id_", drone_id_);
    if(drone_id_.length() == 0)
    {
      drone_id_="7";
    }
    ros::param::get("~drone_id_namespace", drone_id_namespace);
    if(drone_id_namespace.length() == 0)
    {
      drone_id_namespace="drone7";
    }
    ros::param::get("~IBVS_config_file", ibvsconfigFile);
    if ( ibvsconfigFile.length() == 0)
    {
        ibvsconfigFile="ibvs_onepoint.xml";
    }
    ros::param::get("~detector_config_file", detectorconfigFile);
    if ( detectorconfigFile.length() == 0)
    {
        detectorconfigFile="frame_detector.xml";
    }
    ros::param::get("~image_receiver_topic_name",image_receiver_topic_name);
    if(image_receiver_topic_name.length() == 0)
    {
      image_receiver_topic_name="/hummingbird_adr1/camera_front/image_raw";
    }
    ros::param::get("~camera_info_topic_name",camera_info_topic_name);
    if(camera_info_topic_name.length() == 0)
    {
      camera_info_topic_name="/hummingbird_adr1/camera_front/camera_info";
    }

    ros::param::get("~detector_image_topic_name",detector_image_topic_name);
    if(detector_image_topic_name.length() == 0)
    {
      detector_image_topic_name="detector_img";
    }
    ros::param::get("~drone_estimated_pose_topic_name",drone_estimated_pose_topic_name);
    if(drone_estimated_pose_topic_name.length() == 0)
    {
      drone_estimated_pose_topic_name="EstimatedPose_droneGMR_wrt_GFF";
    }
    ros::param::get("~pitchroll_cmd_topic_name",pitchroll_cmd_topic_name);
    if(pitchroll_cmd_topic_name.length() == 0)
    {
      pitchroll_cmd_topic_name="command/pitch_roll";
    }
    ros::param::get("~daltitude_cmd_topic_name",daltitude_cmd_topic_name);
    if(daltitude_cmd_topic_name.length() == 0)
    {
      daltitude_cmd_topic_name="command/dAltitude";
    }
    ros::param::get("~dyaw_cmd_topic_name",dyaw_cmd_topic_name);
    if(dyaw_cmd_topic_name.length() == 0)
    {
      dyaw_cmd_topic_name="command/dYaw";
    }
    ros::param::get("~ibvs_image_topic_name",ibvs_image_topic_name);
    if(ibvs_image_topic_name.length() == 0)
    {
      ibvs_image_topic_name="ibvs_output";
    }

    ros::param::get("~drone_command_highlevel_topic_name",drone_command_highlevel_topic_name);
    if(drone_command_highlevel_topic_name.length() == 0)
    {
      drone_command_highlevel_topic_name="command/high_level";
    }

    pitch = 0;
    roll = 0;
    dz = 0;
    dyaw = 0;
    centered = false;


}


bool BehaviorIBVSOnePoint::readConfigs(std::string configFile)
{

    try
    {

    XMLFileReader my_xml_reader(configFile);

    rotate_sign = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:Rotate_Sign");

    /*********************************  Dz Controller ************************************************/

    // Gain
    pid_dz_kp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Gain:Kp");
    pid_dz_ki = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Gain:Ki");
    pid_dz_kd = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Gain:Kd");
    pid_dz_enablesat = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Saturation:enable_saturation");
    pid_dz_satmax = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Saturation:SatMax");
    pid_dz_satmin = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Saturation:SatMin");
    pid_dz_enableantiwp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Anti_wind_up:enable_anti_wind_up");
    pid_dz_kw = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:Anti_wind_up:Kw");
    pid_dz_enableff = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:FeedForward:enable_feedforward");
    pid_dz_ffmass = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:FeedForward:mass");
    pid_dz_fffactor = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dz:FeedForward:factor");

    /*********************************  Roll Controller  **************************************************/

    // Gain
    pid_roll_kp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Gain:Kp");
    pid_roll_ki = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Gain:Ki");
    pid_roll_kd = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Gain:Kd");
    pid_roll_enablesat = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Saturation:enable_saturation");
    pid_roll_satmax = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Saturation:SatMax");
    pid_roll_satmin = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Saturation:SatMin");
    pid_roll_enableantiwp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Anti_wind_up:enable_anti_wind_up");
    pid_roll_kw = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Roll:Anti_wind_up:Kw");

    /*********************************  Dyaw Controller  **************************************************/

    // Gain
    pid_dyaw_kp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Gain:Kp");
    pid_dyaw_ki = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Gain:Ki");
    pid_dyaw_kd = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Gain:Kd");
    pid_dyaw_enablesat = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Saturation:enable_saturation");
    pid_dyaw_satmax = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Saturation:SatMax");
    pid_dyaw_satmin = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Saturation:SatMin");
    pid_dyaw_enableantiwp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Anti_wind_up:enable_anti_wind_up");
    pid_dyaw_kw = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Dyaw:Anti_wind_up:Kw");

    /*********************************  Pitch Controller  **************************************************/

    // Gain
    pid_pitch_kp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Gain:Kp");
    pid_pitch_ki = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Gain:Ki");
    pid_pitch_kd = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Gain:Kd");
    pid_pitch_enablesat = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Saturation:enable_saturation");
    pid_pitch_satmax = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Saturation:SatMax");
    pid_pitch_satmin = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Saturation:SatMin");
    pid_pitch_enableantiwp = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Anti_wind_up:enable_anti_wind_up");
    pid_pitch_kw = (float) my_xml_reader.readDoubleValue("IBVS_OnePointControl:PID_Pitch:Anti_wind_up:Kw");
    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;
}

//ownSetUp()
void BehaviorIBVSOnePoint::ownSetUp()
{
    init();

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+drone_id_+"/"+ibvsconfigFile);
    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }
    std::cout << "Constructor: BehaviorIBVSOnePoint...Exit" << std::endl;
    dynamic_detector = DynamicDetector(stackPath+"/configs/drone"+drone_id_+"/"+detectorconfigFile);

    ROS_INFO("STATE: READY_TO_START");
}

//ownStart()
void BehaviorIBVSOnePoint::ownStart()
{

    // Subscriber
    camera_info_sub = nIn.subscribe(camera_info_topic_name,1,&BehaviorIBVSOnePoint::CamInfoCb,this);
    image_sub = nIn.subscribe(image_receiver_topic_name, 1, &BehaviorIBVSOnePoint::imageCb,this);
    estimated_yaw_sub = nIn.subscribe(drone_estimated_pose_topic_name, 1, &BehaviorIBVSOnePoint::droneEstimatedPoseCallback, this);


    // Publisher
    pub_attitude = nIn.advertise<droneMsgsROS::dronePitchRollCmd>(pitchroll_cmd_topic_name, 1);
    pub_daltitude = nIn.advertise<droneMsgsROS::droneDAltitudeCmd>(daltitude_cmd_topic_name, 1);
    pub_dyaw = nIn.advertise<droneMsgsROS::droneDYawCmd>(dyaw_cmd_topic_name, 1);
    pub_image = nIn.advertise<sensor_msgs::Image>(ibvs_image_topic_name, 1);
    pub_dronecmd = nIn.advertise<droneMsgsROS::droneCommand>(drone_command_highlevel_topic_name,1,true);


    //Configure PIDs
//    PID_Dz.setGains(pid_dz_kp,pid_dz_ki,pid_dz_kd);
    PID_Dz.enableMaxOutput(pid_dz_enablesat,pid_dz_satmin,pid_dz_satmax);
    PID_Dz.enableAntiWindup(pid_dz_enableantiwp,pid_dz_kw);
    PID_Dz.enableFeedForward(pid_dz_enableff,pid_dz_ffmass,pid_dz_fffactor);


//    PID_Roll.setGains(pid_roll_kp,pid_roll_ki,pid_roll_kd);
    PID_Roll.enableMaxOutput(pid_roll_enablesat,pid_roll_satmin,pid_roll_satmax);
    PID_Roll.enableAntiWindup(pid_roll_enableantiwp,pid_roll_kw);

    PID_Dyaw.setGains(pid_dyaw_kp,pid_dyaw_ki,pid_dyaw_kd);
    PID_Dyaw.enableMaxOutput(pid_dyaw_enablesat,pid_dyaw_satmin,pid_dyaw_satmax);
    PID_Dyaw.enableAntiWindup(pid_dyaw_enableantiwp,pid_dyaw_kw);

    PID_Pitch.setGains(pid_pitch_kp,pid_pitch_ki,pid_pitch_kd);
    PID_Pitch.enableMaxOutput(pid_pitch_enablesat,pid_pitch_satmin,pid_pitch_satmax);
    PID_Pitch.enableAntiWindup(pid_pitch_enableantiwp,pid_pitch_kw);

    // Reset PID
    PID_Dyaw.reset();
    PID_Dz.reset();
    PID_Roll.reset();
    PID_Pitch.reset();
    centered = false;
    move = false;
    moving = false;
    detected = false;
    iterator = 0;
    done = false;
    hovering = false;
    frames = 0.0;
    image_dist = 0.75*480.0;

    std_msgs::Header header;
    header.frame_id = "behavior_ibvs_onepoint";
    droneMsgsROS::droneCommand command_msg;
    command_msg.command = droneMsgsROS::droneCommand::MOVE;
    pub_dronecmd.publish(command_msg);

    ROS_INFO("STATE: STARTED");
}


//ownStop()
void BehaviorIBVSOnePoint::ownStop()
{

    std_msgs::Header header;
    header.frame_id = "behavior_land";

    droneMsgsROS::droneCommand msg;
    msg.header = header;
    msg.command = droneMsgsROS::droneCommand::HOVER;
    pub_dronecmd.publish(msg);
    point.x = ref_point.x;
    point.y = ref_point.y;
    centered = false;

    dz = 0.0;
    roll = 0.0;
    pitch = 0.0;
    dyaw = 0.0;
}

//!ownRun()
void BehaviorIBVSOnePoint::ownRun()
{

    if(timerIsFinished()){
        BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::TIME_OUT);
        BehaviorProcess::setFinishConditionSatisfied(true);
        return;
    }

    IBVSOnePoint();
}

void BehaviorIBVSOnePoint::droneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg) {
    yaw = msg.yaw;
}
void BehaviorIBVSOnePoint::CamInfoCb(const sensor_msgs::CameraInfo &msg)
{

    width_cam = msg.width;
    height_cam = msg.height;
    ref_point.x = width_cam/2;
    ref_point.y = height_cam/2;

}
void BehaviorIBVSOnePoint::imageCb(const sensor_msgs::ImageConstPtr& msg)
{

    cv::Mat raw_img;
    ros::Time h = msg->header.stamp;
    frames++;

    cv::Point2f center;
    cv::Point2f corner;

    raw_img = cv_bridge::toCvShare(msg, "bgr8")->image;
    dynamic_detector.nextFrame(raw_img,h.toSec());
    passDynamic(h.toSec());


    if(dynamic_detector.detected())
    {

        point = dynamic_detector.getCenter();
        corner = dynamic_detector.getFrameCorner();
        center = dynamic_detector.getRotationCenter();
        if ((corner.y - center.y) > 0){
          image_dist = corner.y-center.y;
        }

        factor_gains = 0.01*height_cam/image_dist;

        iterator++;



        circle(raw_img,point,10,cv::Scalar(255,0,0),5);
        circle(raw_img,ref_point,8,cv::Scalar(0,255,0),4);
        circle(raw_img,dynamic_detector.getA(),8,cv::Scalar(0,255,255),4);
        circle(raw_img,dynamic_detector.getB(),8,cv::Scalar(0,0,255),4);
        circle(raw_img,dynamic_detector.getFrameCorner(),8,cv::Scalar(0,0,0),4);
        sensor_msgs::ImagePtr detector_img_out = cv_bridge::CvImage(std_msgs::Header(), "bgr8", raw_img).toImageMsg();
        pub_image.publish(detector_img_out);
    }
    if (iterator > 5) detected = true;
    else return;

}


void BehaviorIBVSOnePoint::IBVSOnePoint()
{

    if ((detected) || moving)
    {
        if (!hovering){
            drone_cmd_msg.command = droneMsgsROS::droneCommand::MOVE;
            pub_dronecmd.publish(drone_cmd_msg);
          }

//        if (abs(point.x-ref_point.x) < 15 && abs(point.y-ref_point.y) < 15 && abs(dz) < 0.1 && abs(roll) < 0.002){// && abs(dyaw) < 0.001 ){
//            centered = true;
//        }

//        if ((abs(point.x-ref_point.x) > 50 || abs(point.y-ref_point.y) > 50) || (abs(roll) > 0.1) ){// || abs(dyaw) > 0.01)|| abs(dz) > 0.75 ){
//            centered = false;
//        }
        /*********************************  Adapt Gains  **********************************************/
        PID_Dz.setGains(pid_dz_kp*factor_gains,pid_dz_ki*factor_gains,pid_dz_kd*factor_gains);
        PID_Roll.setGains(pid_roll_kp*factor_gains,pid_roll_ki*factor_gains,pid_roll_kd*factor_gains);
    //    PID_Pitch.setGains(pid_pitch_kp*factor_gains,pid_pitch_ki*factor_gains,pid_pitch_kd*factor_gains);


        if(dynamic_detector.detected()){
            /*********************************  Dz Controller  ************************************************/

            PID_Dz.setReference(ref_point.y);
            PID_Dz.setFeedback(point.y);
            dz = PID_Dz.getOutput();


            /*********************************  Roll Controller  **************************************************/

            PID_Roll.setReference(ref_point.x);
            PID_Roll.setFeedback(point.x);
            roll = - PID_Roll.getOutput();

          }else{

            dz = 0;
            roll = 0;
            PID_Roll.reset();
            PID_Dz.reset();

          }



        /*********************************  Dyaw Controller  **************************************************/

        PID_Dyaw.setReference(0.0); // value taken from IBVS 4 points
        PID_Dyaw.setFeedback(yaw);
        dyaw = - PID_Dyaw.getOutput();


        /*********************************  Pitch Controller  **************************************************/

//        if (centered){
//            PID_Pitch.setReference(7*height_cam/10);
//            PID_Pitch.setFeedback(image_dist);
//            pitch = - PID_Pitch.getOutput();

//        }else{
//            PID_Pitch.reset();
//            pitch = 0.0;
//        }
          PID_Pitch.setReference(5.0*height_cam/10.0);
          PID_Pitch.setFeedback(image_dist);
          pitch = - PID_Pitch.getOutput();

          std::cout<< "error x "<< fabs(point.x-ref_point.x)<<std::endl
                   << "error y "<<fabs(point.y-ref_point.y)<< std::endl
                   << "error pitch "<< fabs(image_dist-5.0*height_cam/10.0) << std::endl;

          if ((fabs(point.x-ref_point.x) < 20) && (fabs(point.y-ref_point.y) < 25) && (fabs(image_dist-5.0*height_cam/10.0) < 20) && !moving && (frames > 100))
            {
              std::cout<<"hover"<<std::endl;
              hovering = true;
              std_msgs::Header header;
              header.frame_id = "behavior_ibvs_onepoint";
              drone_cmd_msg.command = droneMsgsROS::droneCommand::HOVER;
              drone_cmd_msg.header = header;
              pub_dronecmd.publish(drone_cmd_msg);
            }else if ((fabs(point.x-ref_point.x) > 50) || (fabs(point.y-ref_point.y) > 50) || (fabs(image_dist-5.0*height_cam/10.0) > 60) || moving)
            {
              hovering = false;
            }


        if(move && hovering){
            moving = true;
            move =false;
            hovering = false;
            timer = ros::Time::now().toSec();
        }
        if (moving && (ros::Time::now().toSec() - timer) < 4.0){
            std_msgs::Header header;
            header.frame_id = "behavior_ibvs_onepoint";
            drone_cmd_msg.command = droneMsgsROS::droneCommand::MOVE;
            drone_cmd_msg.header = header;
            pub_dronecmd.publish(drone_cmd_msg);
            pitch = - 0.20;
            roll = 0;
            dz = 0;
            dyaw = 0;
        }else{
            if (moving) done=true;
            moving = false;
        }

        if (done)
        {
            std_msgs::Header header;
            header.frame_id = "behavior_ibvs_onepoint";
            drone_cmd_msg.command = droneMsgsROS::droneCommand::HOVER;
            drone_cmd_msg.header = header;
            pub_dronecmd.publish(drone_cmd_msg);

            BehaviorProcess::setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
            BehaviorProcess::setFinishConditionSatisfied(true);
            return;
        }

    }
    else{

        // Reset PID
        dz = 0.0;
        roll = 0.0;
        pitch = 0.0;
        dyaw = 0.0;
        centered = false;


        PID_Dz.reset();
        PID_Roll.reset();
        PID_Dyaw.reset();
        PID_Pitch.reset();
        drone_cmd_msg.command = droneMsgsROS::droneCommand::HOVER;
        pub_dronecmd.publish(drone_cmd_msg);
    }

    if (hovering){
        dz = 0.0;
        roll = 0.0;
        pitch = 0.0;
        dyaw = 0.0;
      }


    droneMsgsROS::dronePitchRollCmd AttitudeData;

    AttitudeData.rollCmd = roll; //roll data
    AttitudeData.pitchCmd = pitch; //pitch data
    pub_attitude.publish(AttitudeData);


    droneMsgsROS::droneDAltitudeCmd DAltitudeData;

    DAltitudeData.dAltitudeCmd = dz; //dz data
    pub_daltitude.publish(DAltitudeData);

//    droneMsgsROS::droneDYawCmd DyawData;

//    DyawData.dYawCmd = dyaw; //dyaw data
//    pub_dyaw.publish(DyawData);

}
void BehaviorIBVSOnePoint::passDynamic(double time)
{
    //! Calculate for UAV
    // Time to reach the gate at pitch -0.20
    float t = 3.00;
    //! Calculate for dynamic gate
    // Getting angular vel
    float w = dynamic_detector.getW() * 2 * M_PI;
//    std::cout<< "angle= "<<dynamic_detector.getAngle(time)<<std::endl;

    // Angle done while UAV reach the gate
    float alpha = w * t;

    //Adapt alpha to range [0,2PI]
    while (alpha >= 2*M_PI){
        alpha -= 2*M_PI;
    }
    while (alpha <= 0){
        alpha += 2*M_PI;
    }
//    std::cout<<"T: " <<dynamic_detector.getT()<<std::endl;

//    std::cout<<"angle: "<<dynamic_detector.getAngle(time)<<std::endl;
//    std::cout<<"---------------"<<std::endl;

    // Ideal alpha -> 7/4 * M_PI when reaching the gate

    float angle = 7.0/4.0 * M_PI + rotate_sign * M_PI/4.0 + rotate_sign * alpha;

     // Angle to start moving counterclockwise

    while (angle >= (2*M_PI)){
        angle -= 2*M_PI;
    }
    while (angle <= 0){
        angle += 2*M_PI;
    }

    if ((frames > 100) &&fabs(angle-dynamic_detector.getAngle(time))<0.2){
        move = true;
//        std::cout<<"Angle correct"<<std::endl;
//        std::cout<<"alpha: "<< alpha<< std::endl;
//        std::cout<<"Rotate: "<<rotate_sign<<std::endl;
//        std::cout<<dynamic_detector.getAngle(time)*180/M_PI<<std::endl;
//        std::cout<< "w= "<<w<<std::endl;
//        std::cout<< "T= "<<dynamic_detector.getT()<<std::endl;
//        std::cout<< "angle= "<<angle*180/M_PI<<std::endl;

//        std::cout<<"-----------"<<std::endl;


    }else{
        move = false;
//        std::cout<<"Angle not correct"<<std::endl;

    }

}

std::tuple<bool,std::string> BehaviorIBVSOnePoint::ownCheckActivationConditions()
{
  droneMsgsROS::ConsultBelief query_service;
  std::ostringstream capturador;
  capturador << "battery_level(self,LOW)";
  std::string query(capturador.str());
  query_service.request.query = query;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Battery low, unable to perform action");
    //return false;
  }
  std::ostringstream capturador2;
  capturador2<<"flight_state(self,LANDED)";
  std::string query2(capturador2.str());
  query_service.request.query = query2;
  query_client.call(query_service);
  if(query_service.response.success)
  {
    return std::make_tuple(false,"Error: Drone landed");
    //return false;
  }

  return std::make_tuple(true,"");
  //return true;
}
