#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>
#include <deque>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>
#include <cstdlib>
#include "detector.h"
using namespace std;
using namespace cv;
 #include "xmlfilereader.h"

    DynamicDetector::DynamicDetector( string configfile){

    XMLFileReader my_xml_reader(configfile);

    //binarization thresholds orange
    detectorOrange.low_H = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Hue:Low");
    detectorOrange.low_S = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Saturation:Low");
    detectorOrange.low_V = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Value:Low");
    detectorOrange.high_H = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Hue:High");
    detectorOrange.high_S = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Saturation:High");
    detectorOrange.high_V = my_xml_reader.readDoubleValue("FrameDetector:GateBinarization:Value:High");
    //binarization thresholds orange
    detectorGreen.low_H_g = my_xml_reader.readDoubleValue("FrameDetector:StickBinarization:Hue:Low");
    detectorGreen.low_S_g = my_xml_reader.readDoubleValue("FrameDetector:StickBinarization:Saturation:Low");
    detectorGreen.low_V_g = my_xml_reader.readDoubleValue("FrameDetector:StickBinarization:Value:Low");
    detectorGreen.high_H_g = my_xml_reader.readDoubleValue("FrameDetector:StickBinarization:Hue:High");
    detectorGreen.high_S_g = my_xml_reader.readDoubleValue("FrameDetector:StickBinarization:Saturation:High");
    detectorGreen.high_V_g = my_xml_reader.readDoubleValue("FrameDetector:StickBinarization:Value:High");

}


double norm(Point a ){
    return sqrt( pow(a.x,2) + pow(a.y,2));
}
bool compareContours(vector<Point> a, vector<Point> b)
{
    Rect bb1 = boundingRect(a);
    Rect bb2 = boundingRect(b);
    return bb1.height * bb1.width > bb2.height * bb2.width;
}

double euclideanDistance(Point a, Point b)
{
    return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}
double mean(double a, double b)
{
    return (a + b) / 2;
}
double mean(Buffer<float> a){
double mean;
    for (int i = 1; i < a.size(); i++)
    {
        mean += a.get(i);
    }
    mean /= a.size() ;
    return mean;

}
double deviation(Buffer<float> b)
{
    double mean, std;
    for (int i = 1; i < b.size(); i++)
    {
        mean += b.get(i);
    }
    mean /= b.size() - 1;
    // cout<<mean<<endl;
    for (int i = 1; i < b.size(); i++)
    {
        std += pow(b.get(i) - mean, 2);
    }
    std /= b.size() - 2;
    return sqrt(std);
}
bool swapPoints(Point a, Point b, Point previousA, Point previousB)
{
    double daa, dab, dba, dbb;
    daa = euclideanDistance(a, previousA);
    dab = euclideanDistance(a, previousB);
    dba = euclideanDistance(b, previousA);
    dbb = euclideanDistance(b, previousB);
    return (mean(daa, dbb) > mean(dba, dab));
}
double deviation(Buffer<Point> b)
{
    double mean, std;
    for (int i = 1; i < b.size(); i++)
    {
        mean += euclideanDistance(b.get(i), b.get(i - 1));
    }
    mean /= b.size() - 2;
    // cout<<mean<<endl;
    for (int i = 1; i < b.size(); i++)
    {
        std += pow(euclideanDistance(b.get(i), b.get(i - 1)) - mean, 2);
    }
    std /= b.size() - 3;
    return sqrt(std);
}

double measureAngle(Point center, Point p)
{ //returns angle in [-PI,PI]
    Point v = p - center;
    double angle = atan2(v.y, v.x);
    if (angle < 0)
        angle += 2 * M_PI;
    return angle;
}
void rotationTracker::findCenter()
{
    double stda, stdb;
    stda = deviation(adistance);
    stdb = deviation(bdistance);
    // cout<<"stdA "<<stda<<" stdb "<<stdb<<endl;
    centerA = stda <= stdb;
} //Measure the std between drifts , the lowest corresponds to center??

void rotationTracker::calculateW()
{
    w = 1 / T;
}

void rotationTracker::newDetection(Point a, Point b, Point anchor,double timestamp)
{

    if (A.empty())
    {
        A.add(a);
        B.add(b);
        time.add(timestamp);
        return;
    }
    if (swapPoints(a, b, A.top(), B.top()))
        swap(a, b);
    A.add(a);
    B.add(b);
    C.add(anchor);
    adistance.add(euclideanDistance(a,anchor));
    bdistance.add(euclideanDistance(b,anchor));
    time.add(timestamp);
    angleA.add(measureAngle(a, b));
    angleB.add(measureAngle(b, a));
    if (A.filled())
    {
        ready = 1;
        findCenter();
        double angle = centerA ? angleA.top() : angleB.top();
        if (angle<((double(145) / 180) * M_PI) & angle>((double(125) / 180) * M_PI))
        {
            if (!loopCounted)
            {
                angleCheckPoint = angle;
                loopCounted = 1;
                if (T == -1)
                {
                    if (lastCycle != -1)
                    {
                        T = (timestamp - lastCycle);
                        nLoops = 1;
                    }
                    else
                    {
                        lastCycle = timestamp;
                    }
                }
                else
                {
                    double totalTime = ((T ) + (timestamp - lastCycle))/2.0;
//                    double totalTime = (T * nLoops) + (timestamp - lastCycle);
//                    nLoops++;
//                    T = totalTime / nLoops;
                    T=totalTime;
                    lastCycle = timestamp;
                }
            }
        }
        else
        {
            loopCounted = 0;
        }

        calculateW();
    }
}

void StickDetector::frame2bw()
{
    Mat frame_HSV;
    cvtColor(frame, frame_HSV, COLOR_BGR2HSV);

    // cvtColor(frame, frame_HSV, COLOR_BGR2Lab);
    inRange(frame_HSV, Scalar(low_H_g, low_S_g, low_V_g), Scalar(high_H_g, high_S_g, high_V_g), BW);
    // dilate(BW, BW, Mat(), Point(-1, -1), dilation, 1, 1);
    // erode(BW, BW, Mat(), Point(-1, -1), erosion, 1, 1);
    auto s = BW.size();

    BW.rowRange(round(s.height * 0.8), s.height) = 0;

    // return frame_threshold;_g
    erode(BW, BW, Mat(), Point(-1, -1), erosion, 1, 1);
    dilate(BW, BW, Mat(), Point(-1, -1), dilation, 1, 1);
    erode(BW, BW, Mat(), Point(-1, -1), dilation / 2, 1, 1);
}

void StickDetector::detect(Mat image)
{
    frame = image;
    frame2bw();
    vector<Point> greenStick;

    vector<vector<Point>> contours;
    Mat contourOutput = BW;
    vector<Vec4i> hierarchy;
    findContours(contourOutput, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    if (!contours.empty())
    {
        sort(contours.begin(), contours.end(), compareContours);
        greenStick = contours[0];

        RotatedRect rc2 = minAreaRect(greenStick);
        //  cout<<rc2.size.width/rc2.size.height<<' '<<rc2.size.width<<' '<<rc2.size.height<<"RATIO   "<<abs(1-rc2.size.width/rc2.size.height)<<endl;
        if (abs(1 - (rc2.size.width / rc2.size.height)) < 0.5)
        {
            a = rc2.center;
            b = rc2.center;
            detected = 1;
            squared = 1;
        }
        else
        {
            float angle = rc2.angle;
            if (rc2.size.width < rc2.size.height)
            {
                angle = 90 + angle;
            }
            angle = (angle / 180) * M_PI;
            float ss = ((rc2.size.height > rc2.size.width) ? rc2.size.height : rc2.size.width) / 2;
            a = rc2.center;
            a.x += ss * cos(angle);
            a.y += ss * sin(angle);
            b = rc2.center;
            b.x -= ss * cos(angle);
            b.y -= ss * sin(angle);
            squared = 0;
            detected = 1;
            l = ss * 2;
        }
    }
    else
    {
        detected = 0;
    }
    // namedWindow("BW2", WINDOW_AUTOSIZE); // Create a window for display.
    // imshow("BW2", BW);
    // waitKey(5);
}

void FrameDetector::frame2bw()
{
    Mat frame_HSV;

    cvtColor(frame, frame_HSV, COLOR_BGR2HSV);
    inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V), BW);
    erode(BW, BW, Mat(), Point(-1, -1), 1, 1, 1);
    dilate(BW, BW, Mat(), Point(-1, -1), 3, 1, 1);
}

void FrameDetector::detect(Mat image, Point c)
{
    frame = image;
    frame2bw();
    vector<vector<Point>> contours;
    Mat contourOutput = BW;
    vector<Vec4i> hierarchy;
    findContours(contourOutput, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    int id = 0;
    for (int i = 1; i < contours.size(); i++)
    {
        if (compareContours(contours[i], contours[id]))
            id = i;
    }
    BW2 = BW * 0;
    drawContours(BW2, contours, id, Scalar(255, 255, 255), CV_FILLED, 8, hierarchy);
    if (c.x <= 1)
        c.x = 1;
    if (c.y <= 1)
        c.y = 1;
    auto s = BW2.size();
    if (c.x >= s.width)
        c.x = s.width - 1;
    try{
    BW2.colRange(0, s.width).rowRange(0, c.y + 1) = 0;
    BW2.colRange(c.x, s.width).rowRange(0, s.height) = 0;}
    catch(Exception e){}
    line(BW2, Point(0, c.y), c, Scalar(255, 255, 255), 1);
    line(BW2, c, Point(c.x, s.height), Scalar(255, 255, 255), 1);
    findContours(BW2, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
    id = -1;
    for (int i = 0; i < contours.size(); i++)
    {
        if (hierarchy[i][3] != -1)
        {
            if (id = -1)
            {
                id = i;
            }
            else
            {
                if (compareContours(contours[i], contours[id]))
                    id = i;
            }
        }
    }
    if (id == -1)
        detected = 0;
    else
    {
        BW2 *= 0;
        drawContours(BW2, contours, id, Scalar(255, 255, 255), 1, 8, hierarchy);
        vector<Point> approx, approxOrdered, hull;
        double epsilon = 0.01 * arcLength(contours[id], true);

        approxPolyDP(contours[id], approx, epsilon, true);
        Point center = (approx[0] + approx[1] + approx[2] + approx[3]) / 4;

        for (int i = 1; i < 4; i++)
        {
            Point a = approx[i] - center;
            if (a.x < 0 && a.y > 0)
                p = approx[i];
        }
        detected = 1;
    }
    // namedWindow("BW", WINDOW_AUTOSIZE); // Create a window for display.
    // imshow("BW", BW);
    // waitKey(5);
}


void DynamicDetector::nextFrame(Mat image, double timestamp)
{
    frame = image;
    detectorGreen.detect(frame);
    detectedFlag= 0;
    if (detectorGreen.state() == 0) return ;
    detectorOrange.detect(frame, detectorGreen.getLower());
    if(detectorOrange.status() ==0) return;
    if(detectorGreen.state() == 1){
        //complete detection
        tracker.newDetection(detectorGreen.getA(), detectorGreen.getB(), detectorOrange.getP(), timestamp);
    }
    else{
        //simulate detection
        tracker.simulateDetection(detectorGreen.getCenter(), detectorGreen.getLenght(), timestamp);
    }
    if (!tracker.isReady()) return;
    detectedFlag=1;
//     if (detectorGreen.state() != 0)
//     {
//         if (detectorGreen.state() == 1)
//             tracker.newDetection(detectorGreen.getA(), detectorGreen.getB(), timestamp);
//         else
//         {
//             if (detectorGreen.state() == -1)
//                 tracker.simulateDetection(detectorGreen.getA(), detectorGreen.getLenght(), timestamp);
//         }
//         if (tracker.isReady())
//         {
//             if(stickCenter != Point(0,0)) {


//             a = tracker.getCenter();
//             b = tracker.getPoint();
//             Point aaux,baux, centerPrevious;
//             aaux = (a-frameCorner);
//             baux = (b- frameCorner);
//             centerPrevious = (stickCenter - frameCorner);
//             double anorm= abs(norm(aaux)  ), bnorm= abs(norm(baux) );

//     bool center_filter3;
//             // cout << center_filter<<'\t'<< center_filter2<<'\t'<< euclideanDistance(aaux,centerPrevious ) <<'\t'<<euclideanDistance(baux,centerPrevious )<<'\t'<<endl;

//             if(swapPoints(a,b,tracker.A.top(), tracker.B.top())){
//                 swap(anorm,bnorm);
//                 swap(a,b);
//             }
//                 A.add(anorm);
//                 B.add(bnorm);
//                         bool center_filter = anorm<bnorm ;
//             aaux = (a+frameCorner)/2;
//             baux = (b+ frameCorner)/2;
//             centerPrevious = (stickCenter + frameCorner)/2;
//             bool center_filter2 = euclideanDistance(aaux,centerPrevious ) < euclideanDistance(baux,centerPrevious );
//             if(A.filled()){

//                 double aa = deviation(A); double bb=deviation(B);
//                 center_filter3 = aa<bb;
// //                                cout<<"deviation Amarillo:"<<aa<<"\t deviation rojo:"<<bb<<"   "<<'\t'<<center_filter3<<endl;//<<'\t'<<center_filter2<<'\t'<<center_filter3<<'\t'<<int(center_filter)+int(center_filter2)+int(center_filter3)<<endl;

//             }
//             int ff=int(center_filter)+int(center_filter2)+int(center_filter3);




// //            stickCenter = (ff>=2? a: b);
//             stickCenter = (center_filter3? a: b);
//             if(centerMean!=Point()){
//             centerMean= (centerMean + ( stickCenter+ frameCorner)/2)/2;
//               }
//             else{
//                 centerMean= ( ( stickCenter+ frameCorner)/2);

//               }
//             }
//             else {
//                 stickCenter=tracker.getCenter();
//             }
//             detectorOrange.detect(frame, stickCenter);

//             if (detectorOrange.status() != 0)
//             {
//                 frameCorner = detectorOrange.getP();
//                 detectedFlag = 1;
//             }
//             else
//                 detectedFlag = 0;
//         }
//     }
//     else
//     {
//         detectedFlag = 0;
//     }
}

double rotationTracker::getAngle(double time)
{
    // calculateW();
    double estimated = -(time - this->time.top()) * w + (centerA ? angleA.top() : angleB.top()); //HANDLE NEGATIVE

    // double estimated = (time - lastCycle) * w + angleCheckPoint;;
    if (estimated <= 0)
        estimated += 2 * M_PI;
    return estimated;
}

void rotationTracker::simulateDetection(Point a, double l, double timestamp)
{
    double angle = getAngle(timestamp);
    Point b = a;
    b.x += l * cos(angle);
    b.y += l * sin(angle);
    newDetection(a, b, C.top(),timestamp);
}
