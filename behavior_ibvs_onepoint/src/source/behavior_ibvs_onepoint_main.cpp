#include "../include/behavior_ibvs_onepoint.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, ros::this_node::getName());
  BehaviorIBVSOnePoint behavior;
  behavior.setUp();
  ros::Rate loop_rate(30);
  while(ros::ok())
  {
    ros::spinOnce();
    behavior.run();
    loop_rate.sleep();
  }

  return 0;
}
