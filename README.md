# Brief

IBVS One Point. This node does IBVS using just one point to center in the image (front camera). It also approach to the object, giving it a pair of points.

# Subscribed Topics

- **image_sub** ([sensor\_msgs/ImageConstPtr](http://docs.ros.org/api/sensor_msgs/html/msg/Image.html)). The Image received by the camera.
- **camera_info_sub** ([sensor\_msgs/CameraInfo](http://docs.ros.org/api/sensor_msgs/html/msg/CameraInfo.html)). Camera Info for knowing widht and height.

# Published Topics

- **pub_attitude** ([droneMsgsROS/dronePitchRollCmd](https://bitbucket.org/Vision4UAV/aerostack/wiki/Ontology%20for%20aerial%20robotics%20process)). It modifies the pitch and roll of the vehicle.
- **pub_daltitude** ([droneMsgsROS/droneDAltitudeCmd](https://bitbucket.org/Vision4UAV/aerostack/wiki/Ontology%20for%20aerial%20robotics%20process)). It modifies the altitude of the vehicle.
- **pub_dyaw** ([droneMsgsROS/droneDYawCmd](https://bitbucket.org/Vision4UAV/aerostack/wiki/Ontology%20for%20aerial%20robotics%20process)). It modifies the yaw of the vehicle.
- **pub_image** ([sensor_msgs/Image](http://docs.ros.org/api/sensor_msgs/html/msg/Image.html)). Publish detection on the image. 
- **pub_dronecmd** ([droneMsgsROS/droneCommand](https://bitbucket.org/Vision4UAV/aerostack/wiki/Ontology%20for%20aerial%20robotics%20process)). Set Low level control Mode (Hover/Move).

# Inherits

RobotProcess

# Contributors

Maintainer: Pablo Santofimia Ruiz (psantofimia@gmail.com)

Author: Pablo Santofimia Ruiz (psantofimia@gmail.com)

# Notes

Need ([libPID_Control](https://bitbucket.org/santofimia/libpid_control)) class.

